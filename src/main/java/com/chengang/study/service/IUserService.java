package com.chengang.study.service;

import com.chengang.study.entity.UserT;

public interface IUserService {  
    public UserT getUserById(int userId);  
}  
