/**
 * @Desc：  
 * @author:chengang
 * @version: 2016年8月25日 下午4:46:13   
 */
package com.chengang.study.service.Imp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.chengang.study.dao.UserTDao;
import com.chengang.study.entity.UserT;
import com.chengang.study.service.IUserService;

@Service("userService")
public class UserServiceImpl implements IUserService {  
    @Resource
    private UserTDao userDao;  
    @Override  
    public UserT getUserById(int userId) {  
        return this.userDao.selectByPrimaryKey(userId);  
    }  
  
} 