
//初始化一级菜单
function initMenu(ctx){
	var firstMenu = $("#bjui-hnav-navbar")[0];
	
	firstMenu.innerHTML = "";
	for(var i=0;i<fm.length;i++){
		var pid = fm[i].id;
		var modelId = fm[i].modelId;
		
			if(fm[i].name=='系统管理'){//（系统管理）的头不一样
				firstMenu.innerHTML += '<li class="nodeRoot dropdown" id="'+pid+'"><a class="dropdown-toggle"   id="m'+pid+'" href="#" data-toggle="dropdown"><i class="fa '+modelId+'_1"></i> '+fm[i].name+' <span class="caret"></span></a>'
	            +'<ul class="dropdown-menu" role="menu">'
				+'<ul id="bjui-hnav-tree'+(i+1)+'" class="sys_settings" data-toggle="ztree" data-on-click="MainMenuClick" data-expand-all="true">'
	            +'<li data-id="'+pid+'" data-pid="0" id="aaa"></li><li class=\"divider\"></li>'
	            +'</ul>'
	            +'</ul>'
	         +'</li>';
			}else{
				
				if(fm[i].name!='运营后台'){
			    firstMenu.innerHTML += '<li class="nodeRoot" id="'+pid+'"><a  id="m'+pid+'" href="javascript:void(0);" data-toggle="slidebar"><i class="fa '+modelId+'_1"></i> '+fm[i].name+'</a>'
			                                +'<ul id="bjui-hnav-tree'+(i+1)+'" class="ztree ztree_main" data-toggle="ztree" data-on-click="MainMenuClick" data-expand-all="true" data-noinit="true">'
			                                +'<li data-id="'+pid+'" data-pid="0">'+fm[i].name+'</li>'
			                                +'</ul>'
			                        +'</li>';
				}
			}
			openSecMenu(pid,i);	
		
    }
	//排序
	sortMenu();
	
}

//初始化二级菜单
function openSecMenu(pid,index){
	for(var k=0;k<sm.length;k++){
		if(sm[k].pid == pid){
			var subSM = sm[k].sub;
			for(var j=0;j<subSM.length;j++){
				var url = subSM[j].url;
				var id = subSM[j].id;
				var modelId = subSM[j].modelId;
				$('#bjui-hnav-tree'+(index+1)).append('<li  data-id="'+id+'"  data-pid="'+pid+'" data-url="'+url+'" data-tabid="'+modelId+'">'+subSM[j].name+'</li>')	
			}
		}
	}	

}

//排序及初始化默认显示
function sortMenu(){
	//递增
	var asc = function(a, b) {
	       return parseInt($(a).attr('id'))> parseInt($(b).attr('id')) ? 1 : -1;
	   }
	//递减
	var desc = function(a, b) {
	       return parseInt($(a).attr('id')) >parseInt( $(b).attr('id')) ? -1 : 1;
	   }

	var sortByInput = function(sortBy) {
	       var sortEle = $('.nodeRoot').sort(sortBy);
	       $('#bjui-hnav-navbar').empty().append(sortEle);
	   	    //初始化默认菜单
	   	    $("#bjui-tree0").empty().append($(sortEle[0]).find('li').clone());
	}

	sortByInput(asc);	
};


